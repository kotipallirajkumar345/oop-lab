#include <iostream>

// Base class
class Animal {
public:
    void sleep() {
        std::cout << "Animal is sleeping\n";
    }

protected:
    void eat() {
        std::cout << "Animal is eating\n";
    }

private:
    void run() {
        std::cout << "Animal is running\n";
    }
};

// Derived class with private inheritance
class DogPrivate : private Animal {
public:
    void doSomething() {
        // Access to public and protected members of base class
        sleep();
        eat();

        // Cannot access private members of base class
        // run();
    }
};

// Derived class with protected inheritance
class CatProtected : protected Animal {
public:
    void doSomething() {
        // Access to public and protected members of base class
        sleep();
        eat();

        // Cannot access private members of base class
        // run();
    }
};

// Derived class with public inheritance
class CowPublic : public Animal {
public:
    void doSomething() {
        // Access to public and protected members of base class
        sleep();
        eat();

        // Cannot access private members of base class
        // run();
    }
};

int main() {
    DogPrivate dog;
    CatProtected cat;
    CowPublic cow;

    // Access to public and protected members of base class through derived class objects
    dog.doSomething();
    cat.doSomething();
    cow.doSomething();

    // Cannot access base class members directly through derived class objects
    // dog.sleep();
    // cat.eat();
    // cow.run();

    return 0;
}
