abstract class Animal {
   // Abstract method (does not have a body)
   public abstract void makeSound();

   // Non-abstract method (has a body)
   public void eat() {
      System.out.println("Animal is eating");
   }
}

class Dog extends Animal {
   public void makeSound() {
      System.out.println("Woof");
   }
}

class Main {
   public static void main(String[] args) {
      Animal myDog = new Dog();
      myDog.makeSound();
      myDog.eat();
   }
}
