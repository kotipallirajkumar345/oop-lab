import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;

public class CollectionsDemo {
  public static void main(String[] args) {
  
    // Creating an ArrayList
    ArrayList<String> names = new ArrayList<String>();
    names.add("Alice");
    names.add("Bob");
    names.add("Charlie");
    System.out.println("Names: " + names);
    
    // Creating a HashSet
    HashSet<Integer> numbers = new HashSet<Integer>();
    numbers.add(1);
    numbers.add(2);
    numbers.add(3);
    System.out.println("Numbers: " + numbers);
    
    // Creating a HashMap
    HashMap<String, Integer> ages = new HashMap<String, Integer>();
    ages.put("Alice", 25);
    ages.put("Bob", 30);
    ages.put("Charlie", 35);
    System.out.println("Ages: " + ages);
    
    // Accessing elements in an ArrayList
    String firstName = names.get(0);
    System.out.println("First name: " + firstName);
    
    // Accessing elements in a HashSet
    boolean hasNumberTwo = numbers.contains(2);
    System.out.println("Has number 2: " + hasNumberTwo);
    
    // Accessing elements in a HashMap
    int charlieAge = ages.get("Charlie");
    System.out.println("Charlie's age: " + charlieAge);
  }
}
