import java.io.File;

public class FileDemo {
    public static void main(String[] args) {
        // Create a new File instance with the specified pathname
        File file1 = new File("C:/Users/admin/Desktop/file.txt");
        
        // Create a new File instance with the specified parent pathname and child pathname
        File file2 = new File("C:/Users/admin/Desktop", "file.txt");
        
        // Check if the file or directory exists
        boolean exists = file1.exists();
        System.out.println("File exists: " + exists);
        
        // Check if the file is a directory
        boolean isDirectory = file1.isDirectory();
        System.out.println("Is directory: " + isDirectory);
        
        // Check if the file is a regular file
        boolean isFile = file1.isFile();
        System.out.println("Is file: " + isFile);
        
        // Return an array of strings representing the files and directories in the directory
        if (isDirectory) {
            String[] files = file1.list();
            System.out.println("Files in the directory:");
            for (String fileName : files) {
                System.out.println(fileName);
            }
        }
    }
}
