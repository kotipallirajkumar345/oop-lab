class A {
    A() {
        System.out.println("Class A is Called");
    }

    int a;
}

class B extends A {

    B() {
        System.out.println("Class B constructor called");
    }

    int b;
}

class C extends B {

    C() {
        System.out.println("Class c constructor called");
    }

    int c;
}

class D extends B,C{

    D() {
        System.out.println("Class D constructor called");
    }

    int d;
}

class diamondprob {
    public static void main(String args[]) {
        D obj = new D();
        obj.a = 10;
        obj.b = 10;
        obj.c = 10;
        obj.d = 10;
    }
}
