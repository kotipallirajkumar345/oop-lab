import java.util.HashMap;
import java.util.Map;

public class MapEx {
  public static void main(String[] args) {
    // Create a new HashMap
    Map<String, Integer> map = new HashMap<>();

    // Add some key-value pairs
    map.put("Alice", 25);
    map.put("Bob", 30);
    map.put("Charlie", 35);

    // Retrieve values based on keys
    System.out.println("Alice's age is " + map.get("Alice"));
    System.out.println("Bob's age is " + map.get("Bob"));
    System.out.println("Charlie's age is " + map.get("Charlie"));

    // Iterate over all key-value pairs
    for (Map.Entry<String, Integer> entry : map.entrySet()) {
      String name = entry.getKey();
      int age = entry.getValue();
      System.out.println(name + " is " + age + " years old.");
    }

    // Check if a key exists in the map
    if (map.containsKey("Alice")) {
      System.out.println("Alice is in the map.");
    } else {
      System.out.println("Alice is not in the map.");
    }

    // Remove a key-value pair from the map
    map.remove("Charlie");
    System.out.println("Charlie's information has been removed from the map.");
  }
}
