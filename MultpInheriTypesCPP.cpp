// Multiple Inheritance Example
#include <iostream>
using namespace std;

// First base class
class Animal {
public:
   void eat() {
      cout << "Eating...";
   }
};

// Second base class
class Bird {
public:
   void fly() {
      cout << "Flying...";
   }
};

// Derived class
class Eagle : public Animal, public Bird {
public:
   void hunt() {
      cout << "Hunting...";
   }
};

// main function
int main() {
   Eagle e;
   e.eat(); // Accessing first base class function
   e.fly(); // Accessing second base class function
   e.hunt(); // Accessing derived class function
   return 0;
}
