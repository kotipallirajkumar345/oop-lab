#include<iostream>
#include<string>

using namespace std;

class Student {
    private:
        string collegeName;
        int collegeCode;
        string fullName;
        double semPercentage;
    public:
        Student(string collegeName, int collegeCode, string fullName, double semPercentage) {
            this->collegeName = collegeName;
            this->collegeCode = collegeCode;
            this->fullName = fullName;
            this->semPercentage = semPercentage;
        }
        
          ~Student() {
        cout << "Object Destroyed!" << endl;
    }

        void display() {
            cout << "College Name: " << collegeName << endl;
            cout << "College Code: " << collegeCode << endl;
            cout << "Full Name: " << fullName << endl;
            cout << "Sem Percentage: " << semPercentage << endl;
        }
};

int main() {
    Student s1("MVGR", 33, "k.mrudula", 85.5);
    s1.display();
    Student s2("MVGR", 33, "k.nani", 100.0);
    s2.display();
    return 0;
}
