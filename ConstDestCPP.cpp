#include<iostream>
#include<string>

using namespace std; 

class Student {
    public:
        string fullName;
        int rollNum;
        double semPercentage;
        string collegeName;
        int collegeCode;
        void details() {
            cout << fullName << endl;
            cout << rollNum << endl;
            cout << semPercentage << endl;
            cout << collegeName << endl;
            cout << collegeCode << endl;
        }
        Student(string Name, int rollnum, double semper, string collegename, int code) {
            fullName=Name;
            rollNum=rollnum;
            semPercentage=semper;
            collegeName=collegename;
            collegeCode=code;
        };
   
    ~Student() {
        cout << "Student object destroyed!" << endl;
    }
};

int main() {
     Student s("k.mrudula", 598, 85.5, "MVGR college", 5678);

    cout << "Name: " << s.fullName << endl;
    cout << "Roll Number: " << s.rollNum << endl;
    cout << "Semester Percentage: " << s.semPercentage << endl;
    cout << "College Name: " << s.collegeName << endl;
    cout << "College Code: " << s.collegeCode << endl;

    return 0;
}
