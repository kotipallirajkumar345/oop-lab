class BoxingUnboxingJava1{  
    public static void main(String args[]){  
      int a=50;  
      Integer a2 = Integer.valueOf(a); //Boxing  
      Integer a3 = 5; //Boxing  
            
      System.out.println(a2+" "+a3);   
      
      Integer i = Integer.valueOf(50);  
      int a4 = i.intValue(); //Unboxing  
            
      System.out.println(a4);  
   }   
}
