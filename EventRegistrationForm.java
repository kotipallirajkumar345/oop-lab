import java.awt.*;
import java.awt.event.*;

public class EventRegistrationForm extends Frame implements ActionListener {
   // Label for each field
   private Label nameLabel;
   private Label emailLabel;
   private Label phoneLabel;
   private Label eventLabel;
   
   // Text field for each input
   private TextField nameField;
   private TextField emailField;
   private TextField phoneField;
   private TextField eventField;
   
   // Submit button
   private Button submitButton;

   public EventRegistrationForm() {
      setTitle("Event Registration Form");
      setLayout(new GridLayout(5, 2));

      nameLabel = new Label("Name:");
      add(nameLabel);
      nameField = new TextField(20);
      add(nameField);

      emailLabel = new Label("Email:");
      add(emailLabel);
      emailField = new TextField(20);
      add(emailField);

      phoneLabel = new Label("Phone:");
      add(phoneLabel);
      phoneField = new TextField(20);
      add(phoneField);

      eventLabel = new Label("Event:");
      add(eventLabel);
      eventField = new TextField(20);
      add(eventField);

      submitButton = new Button("Submit");
      add(submitButton);
      submitButton.addActionListener(this);

      pack();
      setVisible(true);
   }

   public void actionPerformed(ActionEvent e) {
      // Handle submit button click
      String name = nameField.getText();
      String email = emailField.getText();
      String phone = phoneField.getText();
      String event = eventField.getText();

      // Save registration data to database or file
      System.out.println("Registration successful:");
      System.out.println("Name: " + name);
      System.out.println("Email: " + email);
      System.out.println("Phone: " + phone);
      System.out.println("Event: " + event);
   }

   public static void main(String[] args) {
      EventRegistrationForm form = new EventRegistrationForm();
   }
}
