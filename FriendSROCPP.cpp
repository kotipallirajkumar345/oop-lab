#include <iostream>
using namespace std;

class Box {
private:
    float length, width, height;
public:
    void setDimensions(float len, float wid, float hei) {
        length = len;
        width = wid;
        height = hei;
    }
    float calculateArea() {
        return length * width;
    }
    float calculateVolume() {
        return length * width * height;
    }
    void displayDimensions() {
        cout << "The dimensions of the box are " << length << " units by " << width << " units by " << height << " units. " << endl;
    }
};

inline void displayWelcomeMessage() {
    cout << "Welcome to the Box Calculator!" << endl;
}

int main() {
    displayWelcomeMessage();

    Box b1;
    float l, w;
    cout << "Enter the length of the box: ";
    cin >> l;
    cout << "Enter the width of the box: ";
    cin >> w;
    b1.setDimensions(l, w, 0);
    cout << "The area of the box is " << b1.calculateArea() << " square units." << endl;

    Box b2;
    cout << "Enter the length of the box: ";
    cin >> l;
    cout << "Enter the width of the box: ";
    cin >> w;
    float h;
    cout << "Enter the height of the box: ";
    cin >> h;
    b2.setDimensions(l, w, h);
    cout << "The volume of the box is " << b2.calculateVolume() << " cubic units." << endl;
    b2.displayDimensions();

    return 0;
}
