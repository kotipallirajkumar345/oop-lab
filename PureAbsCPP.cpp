#include <iostream>
using namespace std;

class Shape {
public:
  virtual void draw() = 0; // pure virtual function
};

class Circle : public Shape {
public:
  void draw() {
    cout << "Drawing a circle." << endl;
  }
};

class Square : public Shape {
public:
  void draw() {
    cout << "Drawing a square." << endl;
  }
};

int main() {
  Shape* shape1 = new Circle();
  Shape* shape2 = new Square();
  shape1->draw();
  shape2->draw();

  return 0;
}
