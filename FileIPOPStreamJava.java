import java.io.*;

public class FileIPOPStreamJava {
    public static void main(String[] args) {
        try {
            // Create a FileInputStream object for the source file
            FileInputStream source = new FileInputStream("source.txt");
            
            // Create a FileOutputStream object for the destination file
            FileOutputStream destination = new FileOutputStream("destination.txt");
            
            // Create a byte array to hold the contents of the source file
            byte[] buffer = new byte[1024];
            
            // Read bytes from the source file and write them to the destination file
            int bytesRead;
            while ((bytesRead = source.read(buffer)) != -1) {
                destination.write(buffer, 0, bytesRead);
            }
            
            // Close the streams
            source.close();
            destination.close();
            
            System.out.println("File copied successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
