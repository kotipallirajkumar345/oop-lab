#include <iostream>
using namespace std;

// Base class
class Vehicle {
   public:
      void start() {
         cout << "Starting the vehicle" << endl;
      }
      void stop() {
         cout << "Stopping the vehicle" << endl;
      }
};

// Derived class
class Car : public Vehicle {
   public:
      void drive() {
         cout << "Driving the car" << endl;
      }
};

// Main function
int main() {
   // Create an object of Car class
   Car myCar;

   // Call methods of both classes
   myCar.start();
   myCar.drive();
   myCar.stop();

   return 0;
}
