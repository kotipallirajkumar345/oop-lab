interface Shape {
  public double getArea();
}

// Define a concrete implementation of the Shape interface for a rectangle
class Rectangle implements Shape {
  private double length;
  private double width;

  public Rectangle(double length, double width) {
    this.length = length;
    this.width = width;
  }

  public double getArea() {
    return length * width;
  }
}

// Define a concrete implementation of the Shape interface for a circle
class Circle implements Shape {
  private double radius;

  public Circle(double radius) {
    this.radius = radius;
  }

  public double getArea() {
    return Math.PI * radius * radius;
  }
}

// Usage example
public class Main {
  public static void main(String[] args) {
    Shape[] shapes = new Shape[2];
    shapes[0] = new Rectangle(10, 5);
    shapes[1] = new Circle(7);

    for (Shape shape : shapes) {
      System.out.println("Area: " + shape.getArea());
       
    }
     
  }
} 
