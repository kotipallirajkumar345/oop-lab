class Animal {
    public void sleep() {
        System.out.println("Animal is sleeping");
    }

    protected void eat() {
        System.out.println("Animal is eating");
    }

    private void run() {
        System.out.println("Animal is running");
    }
}

class DogPrivate extends Animal {
    public void doSomething() {
        sleep();
        eat();
    }
}

class CatProtected extends Animal {
    public void doSomething() {
        sleep();
        eat();
    }
}

class CowPublic extends Animal {
    public void doSomething() {
        sleep();
        eat();
    }
}

public class PPPInheriJava {
    public static void main(String[] args) {
        DogPrivate dog = new DogPrivate();
        CatProtected cat = new CatProtected();
        CowPublic cow = new CowPublic();

        dog.doSomething();
        cat.doSomething();
        cow.doSomething();
    }
}
