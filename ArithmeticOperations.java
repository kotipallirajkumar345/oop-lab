import java.util.Scanner;
public class ArithmeticOperations {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first number: ");
        int num1 = input.nextInt();

        System.out.print("Enter second number: ");
        int num2 = input.nextInt();

        System.out.println("Select the operations to perform: ");
        System.out.println("1. Addition");
        System.out.println("2. Subtration");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");

        System.out.print("Enter your choice (1/2/3/4): ");
        int n = input.nextInt();

        if(n==1) {
              System.out.println(num1+ "+" + num2+ "=" + (num1 + num2));
            }
        else if(n==2) {
              System.out.println(num1+ "-" + num2+ "=" + (num1 - num2));
            } 
        else if(n==3) {
              System.out.println(num1+ "*" + num2+ "=" + (num1 * num2));
              }
        else if(n==4) {
              System.out.println(num1+ "+" + num2+ "=" + (num1 + num2));
              }
        else{
            System.out.println("Invalid choice.");
        }
    }
}
