import java.awt.*;
import java.awt.event.*;

public class LoginFrame extends Frame implements ActionListener {
    Label userLabel, passLabel;
    TextField userText, passText;
    Button loginButton;

    public LoginFrame() {
        setTitle("Login Page");
        setSize(300, 150);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(3, 2));

        userLabel = new Label("Username:");
        add(userLabel);

        userText = new TextField();
        add(userText);

        passLabel = new Label("Password:");
        add(passLabel);

        passText = new TextField();
        passText.setEchoChar('*');
        add(passText);

        loginButton = new Button("Login");
        loginButton.addActionListener(this);
        add(loginButton);

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String username = userText.getText();
        String password = passText.getText();

        if (username.equals("admin") && password.equals("password")) {
            System.out.println("Login successful!");
        } else {
            System.out.println("Invalid username or password.");
        }
    }

    public static void main(String[] args) {
        LoginFrame loginFrame = new LoginFrame();
    }
}
