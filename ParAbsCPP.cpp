#include <iostream>
using namespace std;
class Abstract{
    public:
    virtual void dosomething()=0;
    void dosomeother()
    {
        cout<<"Hi"<<endl;
    }
};
class child:public Abstract{
    public:
    void dosomething()
    {
        cout<<"Hello"<<endl;
    }
};
int main()
{
    child obj;
    obj.dosomething();
    obj.dosomeother();
}
