#include<iostream>
using namespace std;

// Class definition
class AccessSpecifierDemo {
private:
    int priVar;
protected:
    int proVar;
public:
    int pubVar;

    // Method to set the values of member variables
    void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    // Method to get and display the values of member variables
    void getVar() {
        cout << "Private variable: " << priVar << endl;
        cout << "Protected variable: " << proVar << endl;
        cout << "Public variable: " << pubVar << endl;
    }
};

// Main function
int main() {
    AccessSpecifierDemo obj;    // Object creation
    obj.setVar(10, 20, 30);     // Method call to set member variables
    obj.getVar();               // Method call to get and display member variables
    return 0;
}
