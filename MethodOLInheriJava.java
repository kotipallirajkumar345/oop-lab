class Shape {
    public void draw() {
        System.out.println("Drawing a generic shape");
    }
    
    public void draw(int x, int y) {
        System.out.println("Drawing a generic shape at (" + x + "," + y + ")");
    }
}

class Rectangle extends Shape {
    public void draw() {
        System.out.println("Drawing a rectangle");
    }
    
    public void draw(int x, int y) {
        System.out.println("Drawing a rectangle at (" + x + "," + y + ")");
    }
}

public class Main {
    public static void main(String[] args) {
        Shape shape1 = new Shape();
        shape1.draw();
        shape1.draw(10, 20);
        
        Rectangle rect1 = new Rectangle();
        rect1.draw();
        rect1.draw(30, 40);
        
        Shape shape2 = new Rectangle();
        shape2.draw();
        shape2.draw(50,60);
        
    }
}
