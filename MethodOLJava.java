class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public double add(double a, double b) {
        return a + b;
    }

    public int add(int a, int b, int c) {
        return a + b + c;
    }
}
public class Main{
    public static void main(String[] args){
        Calculator calc = new Calculator();

int result1 = calc.add(10, 20);
double result2 = calc.add(1.5, 2.5);
int result3 = calc.add(10, 20, 30);

System.out.println(result1); // Output: 30
System.out.println(result2); // Output: 4.0
System.out.println(result3); // Output: 60 
    }
}
