// Hierarchical Inheritance Example
#include <iostream>
using namespace std;

// Base class
class Animal {
public:
   void eat() {
      cout << "Eating...";
   }
};

// Derived class 1
class Dog : public Animal {
public:
   void bark() {
      cout << "Barking...";
   }
};

// Derived class 2
class Cat : public Animal {
public:
   void meow() {
      cout << "Meowing...";
   }
};

// main function
int main() {
   Dog d;
   d.eat(); // Accessing base class function
   d.bark(); // Accessing derived class function
   
   Cat c;
   c.eat(); // Accessing base class function
   c.meow(); // Accessing derived class function
   
   return 0;
}
