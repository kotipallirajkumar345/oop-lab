// Single Inheritance Example
#include <iostream>
using namespace std;

// Base class
class Animal {
public:
   void eat() {
      cout << "Eating...";
   }
};

// Derived class
class Dog : public Animal {
public:
   void bark() {
      cout << "Barking...";
   }
};

// main function
int main() {
   Dog d;
   d.eat(); // Accessing base class function
   d.bark(); // Accessing derived class function
   return 0;
}
