// Multi-Level Inheritance Example
#include <iostream>
using namespace std;

// Base class
class Animal {
public:
   void eat() {
      cout << "Eating...";
   }
};

// Derived class 1
class Dog : public Animal {
public:
   void bark() {
      cout << "Barking...";
   }
};

// Derived class 2
class Bulldog : public Dog {
public:
   void bite() {
      cout << "Biting...";
   }
};

// main function
int main() {
   Bulldog b;
   b.eat(); // calling base class method
   b.bark(); // calling derived class method
   b.bite(); // calling own method
   return 0;
}
