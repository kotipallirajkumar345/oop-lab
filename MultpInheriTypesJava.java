interface Animal {
    void eat();
}

interface Dog {
    void bark();
}

class Labrador implements Animal, Dog {
    public void eat() {
        System.out.println("Labrador is eating.");
    }
    public void bark() {
        System.out.println("Labrador is barking.");
    }
}

public class MultpInheriTypesJava {
    public static void main(String[] args) {
        Labrador l = new Labrador();
        l.eat();
        l.bark();
    }
}
