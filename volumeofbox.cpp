#include <iostream>
#include "boxArea.h"
#include "boxVolume.h"

 using namespace std;

int main() {
    float length, width, height;
    cout << "Enter the length of the box: ";
    cin >> length;
    cout << "Enter the width of the box: ";
    cin >> width;
    cout << "Enter the height of the box: ";
    cin >> height;
    cout << "Area of box: "<< boxArea(length, width, height)<<endl;
    cout << "Volume of box: "<< boxVolume(length, width, height)<<endl;

    return 0;
}
