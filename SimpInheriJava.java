class Vehicle {
   public void start() {
      System.out.println("Starting the vehicle");
   }
   public void stop() {
      System.out.println("Stopping the vehicle");
   }
}

class Car extends Vehicle {
   public void drive() {
      System.out.println("Driving the car");
   }
}

public class Main {
   public static void main(String[] args) {
      // Create an object of Car class
      Car myCar = new Car();

      // Call methods of both classes
      myCar.start();
      myCar.drive();
      myCar.stop();
   }
}
