class MyException extends Exception {
  public MyException(String message) {
    super(message);
  }
}

public class Example {
  public static void main(String[] args) {
    int x = 10;
    try {
      if (x > 5) {
        throw new MyException("x should not be greater than 5");
      }
    } catch (MyException e) {
      System.out.println("Caught the exception: " + e.getMessage());
    }
  }
}
