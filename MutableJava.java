public class MutableJava {
    public static void main(String[] args) {
        int a = 10;
        String name = "MVGR";
        StringBuffer section = new StringBuffer("B");
        System.out.println("int Before: " + System.identityHashCode(a));
        a = 11;
        System.out.println("int After: " + System.identityHashCode(a));
        System.out.println("String Before: " + System.identityHashCode(name));
        name = name.concat(" College");
        System.out.println("String After: " + System.identityHashCode(name));
        System.out.println("StringBuffer Before: " + System.identityHashCode(section));
        section.append(" section");
        System.out.println("StringBuffer After: " + System.identityHashCode(section));
    }
}
