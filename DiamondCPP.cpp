#include <iostream>
using namespace std;
 
class A {
public:
  A()  { 
    cout << "Class A constructor called" << endl; 
    }
    int a;
};
 
class B: public A {
public:
  B()  { 
    cout << "Class B constructor called" << endl;
    }
    int b;    
};
 
class C: public A {
public:
  C()  { 
    cout << "Class C constructor called" << endl; 
    }
    int c;
};
 
class D: public B, public C {
public:
  D()  { 
    cout << "Class D constructor called" << endl;
    }
    int d;
};
 
int main() {
    D d;
    //d.a=10;
    d.b=10;
    d.c=10;
    d.d=10;
    cout<<d.b<<endl<<d.c<<endl<<d.d;
}
